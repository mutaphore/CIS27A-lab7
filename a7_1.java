/*
@author: Dewei Chen 
@date: 2-24-2012
@class: CIS27A
@instructor: Dave Harden
@filename: a7_1.java
@description: This program prompts the user to enter a credit card number 
as a long integer and display whether the number is valid or invalid. The
criteria being evaluated to determine if a credit card is valid or not is
the following:
- Number must be between 13 and 16 digits
- Number prefix must start with 4,5,37 or 6
- Sum of the double of the even position digits and the odd position digits
  must be divisible by 10
*/

import java.util.Scanner;

public class a7_1 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		long number;
		
		System.out.print("Enter a credit card number as a long integer (negative number to quit): ");
		number = input.nextLong();
		
		while (number >= 0) {
	        if (isValid(number)) {
	            System.out.println(number + " is valid");
	        } else {
	            System.out.println(number + " is invalid"); 
	        }    
	        System.out.print("Enter a credit card number as a long integer (negative number to quit): ");
	        number = input.nextLong();        
	    }

	}
	
	
	
	
	
	
	
	
	
	

	/** Checks for prefix of the credit card number, the length
	 * of the credit card number and the sum of double and odd 
	 * place digits. Return true if the card number is valid */
	public static boolean isValid(long number) {
		
		boolean valid = false;
		
		if (prefixMatched(number,4) || 
			prefixMatched(number,5) || 
			prefixMatched(number,37) || 
			prefixMatched(number,6)) {
			
			if (getSize(number) >= 13 && getSize(number) <= 16) {
				
				if ((sumOfDoubleEvenPlace(number) + sumOfOddPlace(number)) % 10 == 0)
					valid = true;
				
			}
				
		}
		
		return valid;
		
	}
	
	
	
	
	
	
	
	
	
	

	/** Sums the double of all even place digits by calling method getDigit */
	public static int sumOfDoubleEvenPlace(long number) {
		
		int sum = 0;
		String numString = String.valueOf(number);   
		
		for (int i=getSize(number)-2 ; i>=0 ; i-=2)
			sum += getDigit(2 * Character.getNumericValue(numString.charAt(i)));
			
		return sum;
		
	}

	
	
	
	
	
	
	
	
	
	
	/** Return this number if it is a single digit, otherwise, return the sum of the two digits */
	public static int getDigit(int number) {
		
		int singleDigit;
				
		if (number >= 10)
			singleDigit = 1 + number % 10;
		else
			singleDigit = number;
		
		return singleDigit;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	/** Return sum of odd place digits in number */
	public static int sumOfOddPlace(long number) {
		
		int sum = 0;
		String numString = String.valueOf(number);   
		
		for (int i=getSize(number)-1 ; i>=0 ; i-=2)
			sum += Character.getNumericValue(numString.charAt(i));
		
		return sum;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	/** Return true if the digit d is a prefix for number. Calls method getPrefix */
	public static boolean prefixMatched(long number, int d) {
		
		boolean match;
		
		if (d == getPrefix(number, getSize(d)))
			match = true;
		else
			match = false;
			
		return match;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	/** Return the number of digits in d */
	public static int getSize(long d) {
		
		return String.valueOf(d).length();
		
	}
	
	
	
	
	
	
	
	
	
	
	
	/** Return the first k number of digits from number. If the
	* number of digits in number is less than k, return number. */
	public static long getPrefix(long number, int k) {
		
		long returnNum;
		
		if (getSize(number) < k)
			returnNum = number;
		else
			returnNum =  number / (long) Math.pow(10,(getSize(number)-k));
			
		return returnNum;
		
	}

}